document
  .getElementById("signup-form")
  .addEventListener("submit", function (event) {
    var name = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var confirmPassword = document.getElementById("confirm-password").value;
    var emailPattern = /^[^\s@]+\.gcit@rub\.edu\.bt$/;
    var passwordPattern = /^(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$/;

    // Validate email
    if (!emailPattern.test(email)) {
      event.preventDefault();
      document.getElementById("email-error").innerHTML =
        "Invalid email format. Use .gcit@rub.edu.bt";
    } else {
      document.getElementById("email-error").innerHTML = "";
    }

    // Validate password
    if (!passwordPattern.test(password)) {
      event.preventDefault();
      document.getElementById("password-error").innerHTML =
        "Password must be 8-20 characters with at least one special character (!@#$%^&*)";
    } else {
      document.getElementById("password-error").innerHTML = "";
    }

    // Validate confirm password
    if (password !== confirmPassword) {
      event.preventDefault();
      document.getElementById("confirm-password-error").innerHTML =
        "Passwords do not match.";
    } else {
      document.getElementById("confirm-password-error").innerHTML = "";
    }
  });
