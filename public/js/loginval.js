console.log("hello");
document
  .getElementById("loginForm")
  .addEventListener("submit", function (event) {
    var email = document.getElementById("emailInput").value;
    var password = document.getElementById("passwordInput").value;
    var emailPattern = /^[^\s@]+\.gcit@rub\.edu\.bt$/;

    // Validate email
    if (!emailPattern.test(email)) {
      document.getElementById("email-error").innerHTML =
        "Invalid email format. Use .gcit@rub.edu.bt";
      return false; // Prevent form submission
    } else {
      document.getElementById("email-error").innerHTML = "";
    }

    // Validate password
    if (password.length < 8) {
      document.getElementById("password-error").innerHTML =
        "Password must be at least 8 characters long.";
      return false; // Prevent form submission
    } else {
      document.getElementById("password-error").innerHTML = "";
    }

    // If validation passes, allow the form to submit
    return true;
  });
