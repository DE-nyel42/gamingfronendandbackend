const express = require("express");
const path = require("path");
const bcrypt = require("bcrypt");
const { log } = require("console");
const Users = require("./config");

const app = express();
//convert data into json format
app.use(express.json());

app.use(express.urlencoded({ extended: false }));

//view engine ejs
app.set("view engine", "ejs");
app.use(express.static("public"));

app.get("/", (req, res) => {
  res.render("login");
});

app.get("/signup", (req, res) => {
  res.render("signup");
});

//register user

app.post("/signup", async (req, res) => {
  const data = {
    email: req.body.email,
    name: req.body.name,
    password: req.body.password,
  };

  //check if user exists
  const existingUser = await Users.findOne({ email: data.email });
  if (existingUser) {
    res.send("User already exists. Please sign up using a different email");
  } else {
    // to send data to the database
    //hash the password
    const saltRounds = 10;
    const hashPassword = await bcrypt.hash(data.password, saltRounds);
    data.password = hashPassword;

    const userdata = await Users.insertMany(data);
    console.log(userdata);
    res.render("login");
  }
});

//login user
app.post("/login", async (req, res) => {
  try {
    const check = await Users.findOne({ email: req.body.email });
    if (!check) {
      res.send("No account found with this email address");
    }
    //compare hash password
    const isPasswordMatch = await bcrypt.compare(
      req.body.password,
      check.password
    );
    if (isPasswordMatch) {
      res.render("Main_Dashboard");
    } else {
      req.send("wrong password");
    }
  } catch {
    res.send("wrong details");
  }
});

const port = 5000;
app.listen(port, () => {
  console.log("server running on port:" + port);
});
