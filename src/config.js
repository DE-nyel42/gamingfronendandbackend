const mongoose = require("mongoose");
const connect = mongoose.connect("mongodb://127.0.0.1:27017/GamingWebsite");

//check connection to the database
connect
  .then(() => {
    console.log("database connected succesfully");
  })
  .catch((error) => {
    console.log("Database connection error:", error);
  });

// create a schema

const LogInSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
});

// model
const Users = new mongoose.model("Users", LogInSchema);

module.exports = Users;
